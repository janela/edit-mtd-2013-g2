#import "EDSmoothedBIView.h"
#import <QuartzCore/QuartzCore.h>




@implementation EDSmoothedBIView
{
    
    UIBezierPath *path;
    //UIImage *incrementalImage;
    CGPoint pts[5]; // we now need to keep track of the four points of a Bezier segment and the first control point of the next segment
    uint ctr;
    
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self setMultipleTouchEnabled:NO];
        path = [UIBezierPath bezierPath];
        [path setLineWidth:10.0];
    }
    
    return self;
    
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setMultipleTouchEnabled:NO];
        path = [UIBezierPath bezierPath];
        [path setLineWidth:10.0];
        
        
        
        self.opaque = false;
    }
    
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    UIColor *strokeColor = [UIColor colorWithRed:0.00 green:255.00 blue:255.00 alpha:0];
    [strokeColor setStroke];
    [_incrementalImage drawInRect:rect];
    [path stroke];
    
    path.lineWidth = 10.0;
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    ctr = 0;
    UITouch *touch = [touches anyObject];
    pts[0] = [touch locationInView:self];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint p = [touch locationInView:self];
    ctr++;
    pts[ctr] = p;
    if (ctr == 4) 
    {
        pts[3] = CGPointMake((pts[2].x + pts[4].x)/2.0, (pts[2].y + pts[4].y)/2.0); // move the endpoint to the middle of the line joining the second control point of the first Bezier segment and the first control point of the second Bezier segment 
        
        [path moveToPoint:pts[0]];
        [path addCurveToPoint:pts[3] controlPoint1:pts[1] controlPoint2:pts[2]];
        

       [self setNeedsDisplay];
        // replace points and get ready to handle the next segment
        pts[0] = pts[3]; 
        pts[1] = pts[4]; 
        ctr = 1;
    }
    
    if (_activewrite) {
        //<#statements#>
        
        //NSLog(@"EREASE LINE");
    } else {
        if([_delegate respondsToSelector:@selector(drawElement:)])
        {
            //send the delegate function with the amount entered by the user
            [_delegate drawElement:path];
        }
        //NSLog(@"DRAW LINE");
    }
    
    
    
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    //[self drawBitmap];
    [self setNeedsDisplay];
    //[path removeAllPoints];
    ctr = 0;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesEnded:touches withEvent:event];
}

- (void)drawBitmap
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, YES, 0.0);
    
    if (!_incrementalImage) // first time; paint background white
    {
        UIBezierPath *rectpath = [UIBezierPath bezierPathWithRect:self.bounds];
        
        
        [[UIColor whiteColor] setFill];
        [rectpath fill];
    }
    [_incrementalImage drawAtPoint:CGPointZero];
    //[[UIColor blackColor] setStroke];
    [path stroke];
    _incrementalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

@end


