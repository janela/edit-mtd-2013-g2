//
//  EDUnpublishedWorksVC.h
//  Apprtist
//
//  Created by Jorge Oliveira on 03/07/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface EDUnpublishedWorksVC : UICollectionViewController<UIAlertViewDelegate>

@property(strong,nonatomic) NSMutableArray * userUnpublishedArtworks;

@property(strong,nonatomic) NSString * unpublishedAuthor;

- (IBAction)deleteArtworks:(id)sender;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *editBtn;
@property(strong,nonatomic) UIBarButtonItem * delete;


@property(strong,nonatomic) UIToolbar * toolbar;

@end



