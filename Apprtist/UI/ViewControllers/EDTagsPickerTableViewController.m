//
//  EDTagsPickerTableViewController.m
//  Apprtist
//
//  Created by h4v1nfun on 10/8/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import "EDTagsPickerTableViewController.h"
#import "EDRequest.h"
#import "EDURLConnectionLoader.h"
#import "CJSONDeserializer.h"
#import "EDPublicarImagemViewController.h"

@interface EDTagsPickerTableViewController ()

@property (strong, nonatomic) NSMutableArray *selectedTags;
@property(strong,nonatomic) NSMutableArray * unselectedTags;
@property(strong,nonatomic) NSMutableArray * finalTags;
@end

@implementation EDTagsPickerTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
   //  self.clearsSelectionOnViewWillAppear = NO;
    
  //  if (!self.selectedTags) {
        self.selectedTags = [[NSMutableArray alloc] initWithCapacity:10];
  //  }
    self.tableView.allowsMultipleSelection = YES;
    
    self.unselectedTags = [NSMutableArray new];
    self.finalTags = [NSMutableArray new];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    
    self.TagsNames = [NSMutableArray new];
    EDRequest *pedido = [[EDRequest alloc] init];
    EDURLConnectionLoader *loader = [[EDURLConnectionLoader alloc] initWithRequest:[pedido RequestTags]] ;
    loader.progressBlock = 0;
	loader.completionBlock = ^(NSError *error, NSData * responseData){
        NSError *jsonParseError = NULL;
        NSDictionary *json = [[CJSONDeserializer deserializer] deserializeAsDictionary:responseData error:&jsonParseError];
        if (!jsonParseError) {
            for (NSDictionary *dict in [json objectForKey:@"Data"]) {
                NSMutableDictionary *dicio = [NSMutableDictionary new];
                //NSLog(@"%@",dict);
                // Add tag name to Array
                [dicio setObject:[dict objectForKey:@"Id"] forKey:@"id"];
                [dicio setObject:[dict objectForKey:@"Name"] forKey:@"Nome"];
                [self.TagsNames addObject:dicio];
            }
            [self.tableView reloadData];
        }
	};
	[loader load];
}

-(void)viewWillDisappear:(BOOL)animated {
    if (_delegate != nil) {
        [_delegate DismissedPop:self.selectedTags withUnselected:self.unselectedTags];
        [_delegate tagsName:self.TagsNames];
    }
    
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return self.TagsNames.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    cell.textLabel.text = [[self.TagsNames objectAtIndex:indexPath.row] objectForKey:@"Nome"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSNumber *selectedTag = [[self.TagsNames objectAtIndex:indexPath.row] objectForKey:@"id"];
    [self.unselectedTags removeAllObjects];
    
    if ([self.tableView cellForRowAtIndexPath:indexPath].accessoryType == UITableViewCellAccessoryCheckmark){
        
        [self.selectedTags removeObjectIdenticalTo:selectedTag];
        [self.unselectedTags addObject:selectedTag];
                
        [self.tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
    } else {
                
        [self.tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        [self.selectedTags addObject:selectedTag];
        
    }
    
    [self.selectedTags removeObjectsInArray:self.unselectedTags];
    
}

@end
