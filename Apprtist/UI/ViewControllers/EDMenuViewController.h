//
//  EDMenuViewController.h
//  Apprtist
//
//  Created by Jorge Oliveira on 28/04/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EDMenuViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UITableView *tableView;


@end
