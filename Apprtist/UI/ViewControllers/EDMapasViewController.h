//
//  EDMapasViewController.h
//  Apprtist
//
//  Created by Jorge Oliveira on 28/04/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "EDAnnotation.h"
#import "IDMPhotoBrowser.h"

@interface EDMapasViewController : UIViewController<CLLocationManagerDelegate,MKAnnotation,MKMapViewDelegate,IDMPhotoBrowserDelegate>

//@property(strong,nonatomic)UIButton * menuBtn;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property(nonatomic, strong) NSArray *workArray;

@property (strong, nonatomic) IBOutlet UIButton *logoutButton;
@property (strong, nonatomic) IBOutlet UILabel *FBUserName;

@end
