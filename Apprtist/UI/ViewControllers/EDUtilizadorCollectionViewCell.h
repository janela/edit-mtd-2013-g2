//
//  EDUtilizadorCollectionViewCell.h
//  Apprtist
//
//  Created by Jorge Oliveira on 18/06/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EDUtilizadorCollectionViewCell : UICollectionViewCell


@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *NomeImagem;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *cellactivity;

@end
