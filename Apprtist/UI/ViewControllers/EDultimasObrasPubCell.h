//
//  EDultimasObrasPubCell.h
//  Apprtist
//
//  Created by Afonso Neto on 7/3/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EDultimasObrasPubCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imagem;
@property (strong, nonatomic) IBOutlet UILabel *nomeImage;
@property (strong, nonatomic) IBOutlet UILabel *dataCria;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) IBOutlet UIView *viewToWork;

@end
