//
//  EDImagemCategoriaViewController.m
//  Apprtist
//
//  Created by Jorge Oliveira on 29/04/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <Parse/Parse.h>
#import "EDImagemCategoriaViewController.h"
#import "ECSlidingViewController.h"
#import "EDMenuViewController.h"
#import "EDGaleriaCollectionViewController.h"
#import "EDImagemCategoriaViewCell.h"
#import "EDGaleriaCollectionViewCell.h"
#import "EDAppDelegate.h"
#import "EDRequest.h"
#import "EDURLConnectionLoader.h"
#import "CJSONDeserializer.h"

@interface EDImagemCategoriaViewController ()

@property(nonatomic, strong) EDArtFiles * selectedCategory;
@property(nonatomic, strong) NSArray *workArray;
@property(nonatomic, strong) NSMutableArray *tagArray;

@end

@implementation EDImagemCategoriaViewController

@synthesize menuBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //fazer aparecer o botão de menu na barra de navegação e teve que ser custom para desaparecer a borda e formato de botão típico
    
    [_logoutButton addTarget:self action:@selector(botaoLogout:) forControlEvents:UIControlEventTouchUpInside];

    if ([[PFUser currentUser] objectForKey:@"profile"][@"name"]) {
        self.userLbl.text = [[PFUser currentUser] objectForKey:@"profile"][@"name"];
    }
    
    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 38, 30)];
    
    
    [button setImage:[UIImage imageNamed:@"menuButton.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(revealMenu:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * menu = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    self.navigationItem.leftBarButtonItem = menu;
    
    _artCategory = [NSMutableArray new];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"texture_grain.png"]];
    
    
}

-(void)viewWillAppear:(BOOL)animated {
    
    /*self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicator.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    self.activityIndicator.frame = self.view.frame;
    self.activityIndicator.hidesWhenStopped = YES;
    self.activityIndicator.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.activityIndicator];
    [_activityIndicator startAnimating];*/
    
    
    self.tagArray = [NSMutableArray new];
    EDRequest *pedido = [[EDRequest alloc] init];
    EDURLConnectionLoader *loader = [[EDURLConnectionLoader alloc] initWithRequest:[pedido RequestTags]] ;
    loader.progressBlock = 0;
	loader.completionBlock = ^(NSError *error, NSData * responseData){
        NSError *jsonParseError = NULL;
        NSDictionary *json = [[CJSONDeserializer deserializer] deserializeAsDictionary:responseData error:&jsonParseError];
        if (!jsonParseError) {
            for (NSDictionary *dict in [json objectForKey:@"Data"]) {
                NSMutableDictionary *dicio = [NSMutableDictionary new];
                //NSLog(@"%@",dict);
                // Add tag name to Array
                [dicio setObject:[dict objectForKey:@"Name"] forKey:@"Nome"];
                [dicio setObject:[dict objectForKey:@"Works"] forKey:@"Trabalhos"];
                [self.tagArray addObject:dicio];
            }
            [_activityIndicator stopAnimating];
            [_activityIndicator setHidden:YES];
            [self.collectionView reloadData];
        }
	};
	[loader load];
}

- (IBAction)botaoLogout:(id)sender {
    [PFUser logOut];
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
     self.slidingViewController.topViewController = newTopViewController;
    
    self.logoutButton.hidden = YES;

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

#pragma mark - CollectionView
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.tagArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString * identifier = @"CellCategoria";
    
    EDImagemCategoriaViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    NSDictionary *tagDict = [NSDictionary dictionaryWithDictionary:[self.tagArray objectAtIndex:indexPath.row]];
    [cell.activityIndicator startAnimating];
    
    cell.categoryLbl.text = [tagDict objectForKey:@"Nome"];
    NSString * imageName = [NSString stringWithFormat:@"%@.jpg",[tagDict objectForKey:@"Nome"]];
    
    cell.imageView.image = [UIImage imageNamed:[imageName lowercaseString]];
    cell.viewToWork.layer.shadowOffset = CGSizeMake(3.0, 3.0);
    cell.viewToWork.layer.shadowColor = [UIColor blackColor].CGColor;
    cell.viewToWork.layer.shadowOpacity = 0.3;
    cell.viewToWork.clipsToBounds = NO;
    
    
    int numberOfPhotos = [[tagDict objectForKey:@"Trabalhos"] count];
    
    cell.numberOfImages.text = [NSString stringWithFormat:@"%d Artworks",numberOfPhotos];
    cell.numberOfImages.textAlignment = NSTextAlignmentRight;
    
    [cell.activityIndicator stopAnimating];

    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *tagDict = [NSDictionary dictionaryWithDictionary:[self.tagArray objectAtIndex:indexPath.row]];
    
    _workArray = [tagDict objectForKey:@"Trabalhos"];
    
    _selectedCategory = [tagDict objectForKey:@"Nome"];
    
    [self performSegueWithIdentifier:@"GaleriaSegue" sender:self];
}

#pragma mark - Navigation
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"GaleriaSegue"]) {
        
        
        EDGaleriaCollectionViewController * gcvc = (EDGaleriaCollectionViewController *)segue.destinationViewController;
        
        gcvc.myCurrentCategory = _selectedCategory;
        gcvc.workArray = _workArray;
    }
}


@end
