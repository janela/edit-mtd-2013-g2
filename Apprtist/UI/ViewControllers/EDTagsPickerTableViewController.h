//
//  EDTagsPickerTableViewController.h
//  Apprtist
//
//  Created by h4v1nfun on 10/8/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TagPickerDelegate <NSObject>
@required
-(void)DismissedPop:(NSArray *)selectedTags withUnselected:(NSArray*)unselectedTags;
-(void)tagsName:(NSArray*)tagsNames;
@end


@interface EDTagsPickerTableViewController : UITableViewController

@property (nonatomic, strong) NSMutableArray *TagsNames;
@property (nonatomic, weak) id<TagPickerDelegate> delegate;

@end
