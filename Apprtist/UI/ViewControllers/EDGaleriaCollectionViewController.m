//
//  EDGaleriaCollectionViewController.m
//  Apprtist
//
//  Created by Jorge Oliveira on 29/04/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <Parse/Parse.h>
#import "EDGaleriaCollectionViewController.h"
#import "ECSlidingViewController.h"
#import "EDMenuViewController.h"
#import "EDGaleriaCollectionViewCell.h"
#import "EDAppDelegate.h"
#import "EDAppSupportTasks.h"
#import "EDRequest.h"
#import "EDURLConnectionLoader.h"
#import "CJSONDeserializer.h"

static NSString * identifier = @"CellGaleria";

@interface EDGaleriaCollectionViewController ()

@property (strong, nonatomic) NSMutableArray *NomeFbArray;
@property (strong, nonatomic) NSMutableArray *arrayTrabalhos;

@end

@implementation EDGaleriaCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"texture_grain.png"]];
    
    self.view.frame = self.view.bounds;
    
    self.title = (NSString*)_myCurrentCategory;
    
    self.NomeFbArray = [NSMutableArray new];
    
    [_logoutButton addTarget:self action:@selector(botaoLogout:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([[PFUser currentUser] objectForKey:@"profile"][@"name"]) {
        self.userLbl.text = [[PFUser currentUser] objectForKey:@"profile"][@"name"];
    }
    
}

-(void) viewWillAppear:(BOOL)animated {
    
    
    /*self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicator.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2-64);
    self.activityIndicator.frame = self.view.frame;
    self.activityIndicator.hidesWhenStopped = YES;
    self.activityIndicator.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.activityIndicator];
    [_activityIndicator startAnimating];*/
    
    EDRequest *pedido = [[EDRequest alloc] init];
    EDURLConnectionLoader *loader = [[EDURLConnectionLoader alloc] initWithRequest:[pedido RequestWorksByWorkIds:self.workArray]];
    loader.progressBlock = 0;
	loader.completionBlock = ^(NSError *error, NSData * responseData){
        NSError *jsonParseError = NULL;
        NSDictionary *json = [[CJSONDeserializer deserializer] deserializeAsDictionary:responseData error:&jsonParseError];
        if (!jsonParseError) {
            self.arrayTrabalhos = [[NSMutableArray alloc] init];
            for (NSDictionary *dict in [json objectForKey:@"Data"]) {
                NSMutableDictionary *dicio = [[NSMutableDictionary alloc] init];
                [dicio setObject:[dict objectForKey:@"Title"] forKey:@"Nome"];
                [dicio setObject:[dict objectForKey:@"File"] forKey:@"Ficheiro"];
                [dicio setObject:[dict objectForKey:@"Latitude"] forKey:@"Latitude"];
                [dicio setObject:[dict objectForKey:@"Longitude"] forKey:@"Longitude"];
                [dicio setObject:[dict objectForKey:@"AuthorFBID"] forKey:@"Autor"];
                [self.arrayTrabalhos addObject:dicio];
            }
            
            [self.collectionView reloadData];
        }
	};
	[loader load];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

- (IBAction)botaoLogout:(id)sender {
    [PFUser logOut];
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
    self.slidingViewController.topViewController = newTopViewController;
    
    self.logoutButton.hidden = YES;
    
    
}

#pragma mark - CollectionView
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_workArray count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    EDGaleriaCollectionViewCell * cell = (EDGaleriaCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    NSDictionary *workDict = [NSDictionary dictionaryWithDictionary:[self.arrayTrabalhos objectAtIndex:indexPath.row]];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Do some stuff off the main queue
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Do something with the UI


            [cell.activityIndicator startAnimating];
            
            [EDAppSupportTasks makeFBRequestForFbId:[workDict objectForKey:@"Autor"] WithCompletionBlock:^(NSError *erro, NSDictionary *result) {
                if (!erro) {
                    [[self.arrayTrabalhos objectAtIndex:indexPath.row] setObject:[result objectForKey:@"name"] forKey:@"NomeAutor"];
                    cell.authorLbl.text = [NSString stringWithFormat:@"%@",[result objectForKey:@"name"]];
                    //[self.activityIndicator stopAnimating];
                } else {
                    NSLog(@"%@",erro);
                }
            }];
            NSURL * url = [NSURL URLWithString:[workDict objectForKey:@"Ficheiro"]];
            
            [cell.thumbGaleria setImageWithURL:url placeholderImage:[UIImage imageNamed:@"face.png"]];
            
            //cell.layer.cornerRadius = 25;
            if ([workDict objectForKey:@"Latitude"] == 0) {
                cell.latitudeLbl.text = @"";
            }else{
                cell.latitudeLbl.text = [NSString stringWithFormat:@"lt: %@",[workDict objectForKey:@"Latitude"]];
            }
            
            if ([workDict objectForKey:@"Longitude"] == 0) {
                cell.longitudeLbl.text = @"";
            }else{
                cell.longitudeLbl.text = [NSString stringWithFormat:@"lg: %@",[workDict objectForKey:@"Longitude"]];
            }
            
            cell.latitudeLbl.textAlignment = NSTextAlignmentRight;
            cell.longitudeLbl.textAlignment = NSTextAlignmentRight;
            
            
            cell.imageViewView.layer.shadowOffset = CGSizeMake(3.0, 3.0);
            cell.imageViewView.layer.shadowOpacity = 0.3;
            cell.imageViewView.layer.shadowColor = [UIColor blackColor].CGColor;
            //cell.imageViewView.clipsToBounds = NO;
            
            [cell.activityIndicator stopAnimating];
            cell.activityIndicator.hidden = YES;
            
        });
    });
    

    
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray * myArray = [NSMutableArray new];
    
    
    
    for (NSDictionary * dict in self.arrayTrabalhos) {
        
        IDMPhoto *photo;
        
        NSURL * url = [NSURL URLWithString:[dict objectForKey:@"Ficheiro"]];
        
        photo = [IDMPhoto photoWithURL:url];
                
        photo.caption = [NSString stringWithFormat:@"%@\n\nby %@",[dict objectForKey:@"Nome"],[dict objectForKey:@"NomeAutor"]];
        
        [myArray addObject:photo];
    }
    
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:myArray];
    browser.delegate = self;
    
    //browser.displayActionButton = NO;
    browser.displayCounterLabel = YES;
    
    // Show
    [self presentViewController:browser animated:YES completion:nil];
    [browser setInitialPageIndex:(NSUInteger)indexPath.row];
    
}


#pragma mark - IDMPhotoBrowser Delegate
- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser didShowPhotoAtIndex:(NSUInteger)index
{
    [photoBrowser photoAtIndex:index];
}



@end
