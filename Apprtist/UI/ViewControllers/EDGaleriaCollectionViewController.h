//
//  EDGaleriaCollectionViewController.h
//  Apprtist
//
//  Created by Jorge Oliveira on 29/04/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDArtFiles.h"
#import "IDMPhotoBrowser.h"

extern NSArray *arrayTrabalhos;


@interface EDGaleriaCollectionViewController: UICollectionViewController<IDMPhotoBrowserDelegate>{

}
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property(strong,nonatomic)UIButton * menuBtn;

- (IBAction)revealMenu:(id)sender;


@property(nonatomic,strong)NSMutableArray * artCategory;

@property(nonatomic,strong) NSArray * imagens;

@property(strong)UIImage * img;

@property(nonatomic, strong) EDArtFiles * myCurrentCategory;
@property(nonatomic, strong) NSArray *workArray;


@property(strong,nonatomic) NSArray * categories;


@property (strong, nonatomic) IBOutlet UILabel *userLbl;


@property (strong, nonatomic) IBOutlet UIButton *logoutButton;

@end




