//
//  EDGaleriaCollectionViewCell.m
//  Apprtist
//
//  Created by Jorge Oliveira on 30/04/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import "EDGaleriaCollectionViewCell.h"


@interface EDGaleriaCollectionViewCell()


@end


@implementation EDGaleriaCollectionViewCell
@synthesize thumbGaleria;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
