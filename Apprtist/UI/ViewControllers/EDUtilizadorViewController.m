//
//  EDUtilizadorViewController.m
//  Apprtist
//
//  Created by Jorge Oliveira on 28/04/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//


#import <Parse/Parse.h>
#import "EDUtilizadorViewController.h"
#import "ECSlidingViewController.h"
#import "EDMenuViewController.h"
#import "EDArtFilesManager.h"
#import "EDArtFiles.h"
#import "EDAppSupportTasks.h"
#import "EDUtilizadorCollectionViewCell.h"
#import "EDRequest.h"
#import "EDURLConnectionLoader.h"
#import "CJSONDeserializer.h"
#import "EDUnpublishedWorksVC.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface EDUtilizadorViewController ()
{
    NSArray * myPhotos;
}

//@property (strong, nonatomic) UIBarButtonItem *logoutButton;
@property (strong, nonatomic) NSMutableDictionary *autorInfo;
//@property (strong, nonatomic) IBOutlet UIBarButtonItem *logoutButtonNew;

@end


@implementation EDUtilizadorViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark - View Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    /*_logoutButton = [[UIBarButtonItem alloc] initWithTitle:@"Log Out"
                                                     style:UIBarButtonItemStyleBordered
                                                    target:self action:@selector(ButaoLogout:)];*/
    
    
    //self.navigationItem.rightBarButtonItem = _logoutButton;
    
    [_logoutButton addTarget:self action:@selector(ButaoLogout:) forControlEvents:UIControlEventTouchUpInside];
    
    self.FBProfilePic.image = [UIImage imageNamed:@"face.png"];
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        [_logoutButton setEnabled:YES];
    } else {
        [_logoutButton setEnabled:NO];
        [_unpublishButton setHidden:YES];
    }
    
    // Butão do menu
    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 38, 30)];
    [button setImage:[UIImage imageNamed:@"menuButton.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(revealMenu:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * menu = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = menu;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"texture_grain.png"]];
    
    _photoPlaceholder.image = [UIImage imageNamed:@"user_photo_placeholder.png"];
    
    myPhotos = [NSArray new];
    myPhotos = [self loadInternalPhotos];
    
    self.unpublishedLbl.text = [NSString stringWithFormat:@"%d",[myPhotos count]];


}

-(NSArray *)loadInternalPhotos
{
    
    NSArray * filePathsArray = [NSArray new];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString * pastaApprtist = [NSString stringWithFormat:@"%@/Apprtist",documentsDirectory];
    filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:pastaApprtist  error:nil];
    
    NSLog(@"%d",[filePathsArray count]);
    
    return filePathsArray;
    
}

-(void) viewWillAppear:(BOOL)animated {
    
   /* self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicator.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    self.activityIndicator.frame = self.view.frame;
    self.activityIndicator.hidesWhenStopped = YES;
    self.activityIndicator.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.activityIndicator];
    [_activityIndicator startAnimating];*/
    [self sendRequestToFB];
    
    myPhotos = [self loadInternalPhotos];
    self.unpublishedLbl.text = [NSString stringWithFormat:@"%d",[myPhotos count]];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

- (IBAction)ButaoLogout:(id)sender {
    [PFUser logOut];
    [_logoutButton setEnabled:NO];
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
    self.slidingViewController.topViewController = newTopViewController;
}

#pragma mark - Facebook Request Methods
-(void)sendRequestToFB {
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        if ([[PFUser currentUser] objectForKey:@"id"]){
            EDRequest *pedido = [[EDRequest alloc] init];
            EDURLConnectionLoader *loader = [[EDURLConnectionLoader alloc] initWithRequest:[pedido RequestAuthorsById:[NSString stringWithFormat:@"%@",[[PFUser currentUser] objectForKey:@"id"]]]];
            loader.progressBlock = 0;
            loader.completionBlock = ^(NSError *error, NSData * responseData){
                NSError *jsonParseError = NULL;
                NSDictionary *json = [[CJSONDeserializer deserializer] deserializeAsDictionary:responseData error:&jsonParseError];
                if (!jsonParseError) {
                    self.autorInfo = [NSMutableDictionary new];
                    NSDictionary *dict = [json objectForKey:@"Data"];
                    // [self.autorInfo setObject:[dict objectForKey:@"Name"] forKey:@"nome"];
                    [self.autorInfo setObject:[dict objectForKey:@"FBID"] forKey:@"idFb"];
                    [self.autorInfo setObject:[dict objectForKey:@"Works"] forKey:@"works"];
                    EDRequest *pedidos = [[EDRequest alloc] init];
                    EDURLConnectionLoader *loadere = [[EDURLConnectionLoader alloc] initWithRequest:[pedidos RequestWorksByWorkIds:[self.autorInfo objectForKey:@"works"]]];
                    loadere.progressBlock = 0;
                    loadere.completionBlock = ^(NSError *error, NSData * responseData){
                        _userPublishedArtworks = [NSMutableArray new];
                        NSError *jsonParseError = NULL;
                        NSDictionary *json = [[CJSONDeserializer deserializer] deserializeAsDictionary:responseData error:&jsonParseError];
                        
                        if (!jsonParseError) {
                            for(NSDictionary *dicio in [json objectForKey:@"Data"]){
                                NSMutableDictionary *objecto= [NSMutableDictionary new];
                                // [self.autorInfo setObject:[dict objectForKey:@"Name"] forKey:@"nome"];
                                [objecto setObject:[dicio objectForKey:@"File"] forKey:@"foto"];
                                [objecto setObject:[dicio objectForKey:@"AuthorFBID"] forKey:@"autor"];
                                [objecto setObject:[dicio objectForKey:@"Title"] forKey:@"nome"];
                                [objecto setObject:[dicio objectForKey:@"Latitude"] forKey:@"latitude"];
                                [objecto setObject:[dicio objectForKey:@"Longitude"] forKey:@"longitude"];
                                [self.userPublishedArtworks addObject:objecto];
                                [self.collectionView reloadData];
                            }
                            
                        }
                    };
                    [loadere load];
                }
            };
            [loader load];
        }
        NSLog(@"Sessão anterior recuperada com sucesso - EDUtilizadorViewController");
        
        [_logoutButton setEnabled:YES];
        [_unpublishButton setHidden:NO];
        // Send request to Facebook
        FBRequest *request = [FBRequest requestForMe];
        [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            // handle response
            //NSLog(@"fez o request ao facebook");
            if (!error) {
                // Parse the data received
                NSDictionary *userData = (NSDictionary *)result;
                                
                NSString *facebookID = userData[@"id"];
                
                NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=200&height=200&return_ssl_resources=1", facebookID]];
                //type=large
                
                NSMutableDictionary *userProfile = [NSMutableDictionary dictionaryWithCapacity:7];
                
                if (facebookID) {
                    userProfile[@"facebookId"] = facebookID;
                }
                
                if (userData[@"name"]) {
                    userProfile[@"name"] = userData[@"name"];
                }
                
                if (userData[@"location"][@"name"]) {
                    userProfile[@"location"] = userData[@"location"][@"name"];
                }
                
                if (userData[@"gender"]) {
                    userProfile[@"gender"] = userData[@"gender"];
                }
                
                if (userData[@"birthday"]) {
                    userProfile[@"birthday"] = userData[@"birthday"];
                }
                
                if ([pictureURL absoluteString]) {
                    userProfile[@"pictureURL"] = [pictureURL absoluteString];
                }
                
                [[PFUser currentUser] setObject:userProfile forKey:@"profile"];
                [[PFUser currentUser] saveInBackground];
                
                
                [self updateFBProfile];
            } else if ([[[[error userInfo] objectForKey:@"error"] objectForKey:@"type"]
                        isEqualToString: @"OAuthException"]) { // Since the request failed, we can check if it was due to an invalid session
                NSLog(@"The facebook session was invalidated");
                [self ButaoLogout:nil];
            } else {
                NSLog(@"Some other error: %@", error);
            }
        }];
        
    } else {
        
        NSLog(@"User nao esta logado - EDUtilizadorViewController");
        
        [_logoutButton setEnabled:NO];
        [_unpublishButton setHidden:YES];
        self.FBUserName.text = @"";
        self.FBGender.text = @"";
        self.FBLocation.text = @"";
        self.IdadeUtilizador.text = @"";
        self.FBProfilePic.image = nil;
    }
}

-(void)updateFBProfile {
    
    
    // Nome do facebook
    
    if ([[PFUser currentUser] objectForKey:@"profile"][@"name"]) {
        self.FBUserName.text = [[PFUser currentUser] objectForKey:@"profile"][@"name"];
        self.largeUsername.text = [[PFUser currentUser] objectForKey:@"profile"][@"name"];
    }
    
    
    // Vai buscar se é Masculino ou Femenino
    if ([[PFUser currentUser] objectForKey:@"profile"][@"gender"]) {
        self.FBGender.text = [NSString stringWithFormat:@"Gender: %@",[[[PFUser currentUser] objectForKey:@"profile"][@"gender"] capitalizedString]];
    }
    
    /*
    // Vê se é maior de idd
    if ([[PFUser currentUser] objectForKey:@"profile"][@"birthday"]) {
        self.IdadeUtilizador.text = [NSString stringWithFormat:@"Age: %d", [EDAppSupportTasks IdadeAPartirDaDataDeNascimento:[[PFUser currentUser] objectForKey:@"profile"][@"birthday"]]];
    }
    */
    
    // Vai buscar a localização
    if ([[PFUser currentUser] objectForKey:@"profile"][@"location"]) {
        self.FBLocation.text = [NSString stringWithFormat:@"City: %@",[[PFUser currentUser] objectForKey:@"profile"][@"location"]];
    }
    
    // Download the user's facebook profile picture
    self.imageData = [[NSMutableData alloc] init]; // the data will be loaded in here
    
    if ([[PFUser currentUser] objectForKey:@"profile"][@"pictureURL"]) {
        NSURL *pictureURL = [NSURL URLWithString:[[PFUser currentUser] objectForKey:@"profile"][@"pictureURL"]];
        
        NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:pictureURL
                                                                  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                              timeoutInterval:2.0f];
        // Run network request asynchronously
        NSURLConnection *urlConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
        if (!urlConnection) {
            NSLog(@"Failed to download picture");
        }
    }
    [self.activityIndicator stopAnimating];
}


#pragma mark - NSURLConnectionDataDelegate

/* Callback delegate methods used for downloading the user's profile picture */

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // As chuncks of the image are received, we build our data file
    [self.imageData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // All data has been downloaded, now we can set the image in the header image view
    self.FBProfilePic.image = [UIImage imageWithData:self.imageData];
    // Add a nice corner radius to the image
    //self.FBProfilePic.layer.cornerRadius = 25;
    self.FBProfilePic.layer.masksToBounds = YES;
}

#pragma mark - CollectionView Delegate

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return self.userPublishedArtworks.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"CellUtilizadorPubslished";
    
    EDUtilizadorCollectionViewCell * cell = (EDUtilizadorCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    [cell.cellactivity startAnimating];
    
     //cell.layer.cornerRadius = 10;
    

    
    NSDictionary *objecto = [_userPublishedArtworks objectAtIndex:indexPath.row];
    
    NSURL * url = [NSURL URLWithString:[objecto objectForKey:@"foto"]];
    
    [cell.imageView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"face.png"]];
   
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Do some stuff off the main queue
        
        NSString *nomeImagem = [NSString stringWithFormat:@"%@",[objecto objectForKey:@"nome"]];
        
        dispatch_async(dispatch_get_main_queue(), ^{

            cell.imageView.layer.shadowOffset = CGSizeMake(3.0, 3.0);
            cell.imageView.layer.shadowColor = [UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:1].CGColor;
            cell.imageView.layer.shadowOpacity = 0.3;
            //cell.imageView.clipsToBounds = NO;
            
            cell.NomeImagem.text = nomeImagem;

            [cell.cellactivity stopAnimating];
            
        });
    });
    
    NSLog(@"%d",self.userPublishedArtworks.count);
    self.publishedLbl.text = [NSString stringWithFormat:@"%d",self.userPublishedArtworks.count];
    
    return cell;

}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray *myArray = [[NSMutableArray alloc] init];
    for (NSDictionary * dict in self.userPublishedArtworks) {
        
        IDMPhoto *photo;
        
        NSURL * url = [NSURL URLWithString:[dict objectForKey:@"foto"]];
        
        photo = [IDMPhoto photoWithURL:url];
        
        photo.caption = [NSString stringWithFormat:@"%@\n\nby %@",[dict objectForKey:@"nome"],[[PFUser currentUser] objectForKey:@"profile"][@"name"]];
        
        [myArray addObject:photo];
        
    }
    
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:myArray];
    browser.delegate = self;
    
    //browser.displayActionButton = NO;
    browser.displayCounterLabel = YES;
    
    // Show
    [self presentViewController:browser animated:YES completion:nil];
    
    //    UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:browser];
    //    [self.navigationController presentModalViewController:navCon animated:YES];
    
    [browser setInitialPageIndex:(NSUInteger)indexPath.row];
}

#pragma mark - IDMPhotoBrowser Delegate
- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser didShowPhotoAtIndex:(NSUInteger)index
{
    [photoBrowser photoAtIndex:index];
}
@end

