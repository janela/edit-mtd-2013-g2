//
//  EDMenuEdicaoViewController.m
//  Apprtist
//
//  Created by Jorge Oliveira on 29/04/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import "EDMenuEdicaoViewController.h"
#import "ECSlidingViewController.h"
#import "EDMediator.h"

@interface EDMenuEdicaoViewController ()

@end

@implementation EDMenuEdicaoViewController

static NSString *activeToolString = nil;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
   // _toolsPencil.gestureRecognizers
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]
                                             initWithTarget:self action:@selector(showGestureForTapRecognizer:)];
    
    // Specify that the gesture must be a single tap
    tapRecognizer.numberOfTapsRequired = 1;
    
    // Add the tap gesture recognizer to the view
    [self.view addGestureRecognizer:tapRecognizer];

    [self.slidingViewController setAnchorLeftPeekAmount:900.0f];
    self.slidingViewController.underRightWidthLayout = ECVariableRevealWidth;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showGestureForTapRecognizer:(UITapGestureRecognizer *)recognizer {
    // Get the location of the gesture
    CGPoint location = [recognizer locationInView:self.view];
    

    
    NSLog(@"%f",location.x);
    // Display an image view at that location
    //[self drawImageForGestureRecognizer:recognizer atPoint:location];
    
    // Animate the image view so that it fades out
    [UIView animateWithDuration:0.5 animations:^{
        self.toolsPencil.alpha = 0.5;
    }];
}
// Method´s for Manage

-(void) manageActiveTools:(NSString*)mstring{
    activeToolString = mstring;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:[[EDMediator sharedMediator] str] object:activeToolString userInfo:nil];
}


// Handler Buttons Tools

- (IBAction)handlePencil:(id)sender {
    [self manageActiveTools :@"Pencil"];
    
    
}

- (IBAction)handlerubber:(id)sender {
    [self manageActiveTools :@"Rubber"];
}

- (IBAction)handleCamera:(id)sender {
    [self manageActiveTools :@"Camera"];
}

- (IBAction)handleAlbum:(id)sender {
    [self manageActiveTools :@"Album"];
}

- (IBAction)handleClear:(id)sender {
    [self manageActiveTools :@"Clear"];
}

- (IBAction)handleSave:(id)sender {
    [self manageActiveTools :@"Save"];
}

- (IBAction)handlePublish:(id)sender {
    [self manageActiveTools :@"Publish"];
}

@end
