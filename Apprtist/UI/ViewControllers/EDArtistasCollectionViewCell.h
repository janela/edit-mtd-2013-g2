//
//  EDArtistasCollectionViewCell.h
//  Apprtist
//
//  Created by Jorge Oliveira on 22/05/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EDArtistasCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *artistaThumb;
@property (strong, nonatomic) IBOutlet UILabel *nomeLabel;
@property (strong, nonatomic) IBOutlet UILabel *apprtistUser;
@property (strong, nonatomic) IBOutlet UILabel *artworkNumber;

@property (strong, nonatomic) IBOutlet UIView *viewToWork;

@end
