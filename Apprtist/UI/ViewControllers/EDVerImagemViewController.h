//
//  EDVerImagemViewController.h
//  Apprtist
//
//  Created by Jorge Oliveira on 29/04/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDArtFiles.h"
#import <QuartzCore/QuartzCore.h>

@interface EDVerImagemViewController : UIViewController <UIGestureRecognizerDelegate>

-(IBAction)verAutor:(id)sender;
-(void)handleTap:(UIGestureRecognizer *)sender;
-(IBAction)handlePinch:(UIPinchGestureRecognizer *)recognizer;
-(IBAction)handlePan:(UIPanGestureRecognizer *)recognizer;
@property (strong, nonatomic) NSString *autor;
@property (strong, nonatomic) NSString *geoTag;
@property (strong, nonatomic) IBOutlet UILabel *appUser;

@property (strong, nonatomic) IBOutlet UILabel *artName;
@property (strong, nonatomic) IBOutlet UILabel *obraGeoTag;

@property(strong, nonatomic)IBOutlet UIImageView *imageView;
@property(strong, nonatomic)IBOutlet UIImageView *fullScreenImageView;

@property(strong)UIImage * img;

@property(nonatomic, strong) EDArtFiles * myCurrentCategory;
@property(nonatomic,strong) NSString * myCurrentSelectedArtName;
@property (strong, nonatomic) IBOutlet UIView *imageViewContainer;
@property (strong, nonatomic) IBOutlet UIView *infoViewContainer;


@property (strong, nonatomic) IBOutlet UIImageView *FBProfilePic;
@property (strong, nonatomic) IBOutlet UILabel *FBUserName;

@end
