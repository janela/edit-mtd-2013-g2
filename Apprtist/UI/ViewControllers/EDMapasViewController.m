//
//  EDMapasViewController.m
//  Apprtist
//
//  Created by Jorge Oliveira on 28/04/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <Parse/Parse.h>
#import "EDMapasViewController.h"
#import "ECSlidingViewController.h"
#import "EDMenuViewController.h"
#import "EDAppDelegate.h"
#import "EDArtFiles.h"
#import "EDVerImagemViewController.h"
#import "EDArtFilesManager.h"
#import "EDAppSupportTasks.h"
#import "EDRequest.h"
#import "EDURLConnectionLoader.h"
#import "CJSONDeserializer.h"

#define METERS_PER_KM 1000

@interface EDMapasViewController ()

@property(nonatomic, strong) NSDictionary * selectedArtwork;
@property(strong,nonatomic) NSMutableDictionary * dictArtWithArtID;
@property(strong,nonatomic) NSMutableArray *locations;
@end

@implementation EDMapasViewController

@synthesize coordinate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //fazer aparecer o botão de menu na barra de navegação e teve que ser custom para desaparecer a borda e formato de botão típico
    
    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 38, 30)];
    
    
    [button setImage:[UIImage imageNamed:@"menuButton.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(revealMenu:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * menu = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    self.navigationItem.leftBarButtonItem = menu;

    [_logoutButton addTarget:self action:@selector(ButaoLogout:) forControlEvents:UIControlEventTouchUpInside];

    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        [_logoutButton setEnabled:YES];
    } else {
        [_logoutButton setEnabled:NO];
    }
    
    if ([[PFUser currentUser] objectForKey:@"profile"][@"name"]) {
        self.FBUserName.text = [[PFUser currentUser] objectForKey:@"profile"][@"name"];
    }

}

- (void)viewWillAppear:(BOOL)animated {
    
    /*self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicator.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    self.activityIndicator.frame = self.view.frame;
    self.activityIndicator.hidesWhenStopped = YES;
    self.activityIndicator.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.activityIndicator];
    [self.activityIndicator startAnimating];*/

    __block CLLocationCoordinate2D location;
    
    EDRequest *pedido = [[EDRequest alloc] init];
    EDURLConnectionLoader *loader = [[EDURLConnectionLoader alloc] initWithRequest:[pedido RequestAllWorks]];
    loader.progressBlock = 0;
	loader.completionBlock = ^(NSError *error, NSData * responseData){
        //CLLocationCoordinate2D coord;
        NSError *jsonParseError = NULL;
        NSDictionary *json = [[CJSONDeserializer deserializer] deserializeAsDictionary:responseData error:&jsonParseError];
        if (!jsonParseError) {
            _dictArtWithArtID = [[NSMutableDictionary alloc] init];
            self.locations = [NSMutableArray new];
            for (NSDictionary * artwork in [json objectForKey:@"Data"]) {
                NSMutableDictionary *dicio = [[NSMutableDictionary alloc] init];
                EDAnnotation * myAnnotation = [[EDAnnotation alloc] init];
                
                myAnnotation.title = [artwork objectForKey:@"Title"];
                myAnnotation.subtitle = [artwork objectForKey:@"Tag"];
                [dicio setObject:[artwork objectForKey:@"Title"] forKey:@"nome"];
                [dicio setObject:[artwork objectForKey:@"File"] forKey:@"file"];
                [dicio setObject:[artwork objectForKey:@"AuthorFBID"] forKey:@"autor"];
                [dicio setObject:[artwork objectForKey:@"Latitude"] forKey:@"lat"];
                [dicio setObject:[artwork objectForKey:@"Longitude"] forKey:@"lon"];
                //myAnnotation.coordinate = CLLocationCoordinate2DMake([[artwork objectForKey:@"Longitude"] doubleValue], [[artwork objectForKey:@"Longitude"] doubleValue]);
                
                myAnnotation.coordinate = location;
                location.latitude = [[artwork objectForKey:@"Latitude"] doubleValue];
                location.longitude = [[artwork objectForKey:@"Longitude"] doubleValue];
                
                [self.locations addObject:myAnnotation];
                [_dictArtWithArtID setObject:dicio forKey:[artwork objectForKey:@"Title"]];
            }
            [self.mapView addAnnotations:self.locations];
            
        }
        [self recenterMap];
        //[self.activityIndicator stopAnimating];
	};
    
	[loader load];
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        [_logoutButton setEnabled:YES];
    } else {
        [_logoutButton setEnabled:NO];
    }
    
    if ([[PFUser currentUser] objectForKey:@"profile"][@"name"]) {
        self.FBUserName.text = [[PFUser currentUser] objectForKey:@"profile"][@"name"];
    }
}

- (IBAction)ButaoLogout:(id)sender {
    [PFUser logOut];
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
    self.slidingViewController.topViewController = newTopViewController;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}



#pragma mark - MapView Methods
- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKPinAnnotationView *mapPin = nil;
    
    if(annotation != map.userLocation)
    {
        static NSString *defaultPinID = @"defaultPin";
        mapPin = (MKPinAnnotationView *)[map dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if (mapPin == nil ){
            
            mapPin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                                     reuseIdentifier:defaultPinID];
            mapPin.canShowCallout = YES;
            
         /*   NSURL *url = [NSURL URLWithString:[[self.dictArtWithArtID objectForKey:annotation.title] objectForKey:@"file"]];
            
            UIGraphicsBeginImageContext(CGSizeMake(100, 100));
            
            [[UIImage imageWithData:[NSData dataWithContentsOfURL:url]] drawInRect:CGRectMake(0.0f, 0.0f, 100, 100)];
            
            // An autoreleased image
            UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
            
            UIGraphicsEndImageContext();
            mapPin.image = newImage; */
            UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            mapPin.rightCalloutAccessoryView = infoButton;
            
        }
        else
            mapPin.annotation = annotation;
        
    }
    return mapPin;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
        
    if ( [view.annotation isKindOfClass:[EDAnnotation class]] ){
        self.selectedArtwork = [[NSDictionary alloc] initWithDictionary:[_dictArtWithArtID objectForKey:view.annotation.title]];
        NSURL *url = [NSURL URLWithString:[self.selectedArtwork objectForKey:@"file"]];
        IDMPhoto *photo = [IDMPhoto photoWithURL:url];
        NSLog(@"%@",self.selectedArtwork);
        photo.caption = [NSString stringWithFormat:@"%@",[self.selectedArtwork objectForKey:@"nome"]];
        NSArray *myArray = @[photo];
        IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:myArray];
        browser.delegate = self;
        [self presentViewController:browser animated:YES completion:nil];
    } 
}

- (void)recenterMap {
    
    NSArray *coordinates = [self.mapView valueForKeyPath:@"annotations.coordinate"];
    
        // look for the minimum and maximum coordinate
        CLLocationCoordinate2D maxCoord = {-90.0f, -180.0f};
        CLLocationCoordinate2D minCoord = {90.0f, 180.0f};
    
        for(NSValue *value in coordinates) {
            
            CLLocationCoordinate2D coord = {0.0f, 0.0f};
            [value getValue:&coord];
            
            if(coord.longitude > maxCoord.longitude) {
                maxCoord.longitude = coord.longitude;
            }
            if(coord.latitude > maxCoord.latitude) {
                maxCoord.latitude = coord.latitude;
            }
            if(coord.longitude < minCoord.longitude) {
                minCoord.longitude = coord.longitude;
            }
            if(coord.latitude < minCoord.latitude) {
                minCoord.latitude = coord.latitude;
            }
        }
    
        // create a region
        MKCoordinateRegion region = {{0.0f, 0.0f}, {0.0f, 0.0f}};
        region.center.longitude = (minCoord.longitude + maxCoord.longitude) / 2.0;
        region.center.latitude = (minCoord.latitude + maxCoord.latitude) / 2.0;
    
        // calculate the span
        region.span.longitudeDelta = maxCoord.longitude - minCoord.longitude+5.25f;
        region.span.latitudeDelta = maxCoord.latitude - minCoord.latitude+5.25f;
    
        // center the map on that region
        [self.mapView setRegion:region animated:YES];

}

#pragma mark - IDMPhotoBrowser Delegate

- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser didDismissAtPageIndex:(NSUInteger)index
{
    id <IDMPhoto> photo = [photoBrowser photoAtIndex:index];
    NSLog(@"Dissmised with photo index: %d, photo caption: %@", index, photo.caption);
    
    
}

- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser didDismissActionSheetWithButtonIndex:(NSUInteger)buttonIndex photoIndex:(NSUInteger)photoIndex
{
    [[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Option %d", buttonIndex+1] message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
    id <IDMPhoto> photo = [photoBrowser photoAtIndex:photoIndex];
    NSLog(@"Dissmised actionSheet with photo index: %d, photo caption: %@", photoIndex, photo.caption);
}

- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser didShowPhotoAtIndex:(NSUInteger)index
{
    [photoBrowser photoAtIndex:index];
}


@end
