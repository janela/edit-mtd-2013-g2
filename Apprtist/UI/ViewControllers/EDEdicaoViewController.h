//
//  EDEdicaoViewController.h
//  Apprtist
//
//  Created by Jorge Oliveira on 28/04/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDMenuEdicaoViewController.h"
#import "EDSmoothedBIView.h"
#import "IDMPhotoBrowser.h"

#define kED_Edicao_Pencil @"Pencil"
#define kED_Edicao_Rubber @"Rubber"
#define kED_Edicao_Camera @"Camera"
#define kED_Edicao_Album @"Album"
#define kED_Edicao_Clear @"Clear"
#define kED_Edicao_Save @"Save"
#define kED_Edicao_Publish @"Publish"

@interface EDEdicaoViewController : UIViewController<DrawDelegate,UIGestureRecognizerDelegate,UIApplicationDelegate,UIPopoverControllerDelegate,UIPickerViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,IDMPhotoBrowserDelegate>

@property (strong, nonatomic) IBOutlet UIView *viewCoolec;
@property (weak, nonatomic) IBOutlet UILabel *textlabelEditview;

@property (strong, nonatomic) IBOutlet UILabel *FBUserName;
@property (strong, nonatomic) IBOutlet UIButton *logoutButton;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) IBOutlet UIView *navigationBarView;
@property (strong, nonatomic) IBOutlet UIImageView *littleMan;
@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
@property (strong, nonatomic) IBOutlet UIButton *revealTools;
@property (strong, nonatomic) IBOutlet UILabel *tipLabel;

-(void)openToReedit:(UIImage*)image;
@end
