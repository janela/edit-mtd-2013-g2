//
//  EDUtilizadorViewController.h
//  Apprtist
//
//  Created by Jorge Oliveira on 28/04/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IDMPhotoBrowser.h"


@interface EDUtilizadorViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,IDMPhotoBrowserDelegate>


@property (strong, nonatomic) IBOutlet UICollectionView* collectionView;

@property (strong, nonatomic) IBOutlet UIImageView *FBProfilePic;
@property (strong, nonatomic) IBOutlet UILabel *FBUserName;
@property (strong, nonatomic) IBOutlet UILabel *largeUsername;

@property (nonatomic, strong) NSMutableData *imageData;
@property (strong, nonatomic) IBOutlet UILabel *FBGender;
@property (strong, nonatomic) IBOutlet UILabel *IdadeUtilizador;
@property (strong, nonatomic) IBOutlet UILabel *FBLocation;


@property (strong, nonatomic) IBOutlet UIButton *unpublishButton;

-(IBAction)ButaoLogout:(id)sender;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;


@property(strong,nonatomic) NSMutableArray * userPublishedArtworks;
@property(strong,nonatomic) NSString * collectionFacebookID;

@property (weak, nonatomic) IBOutlet UIView *noArtworksView;


@property (strong, nonatomic) IBOutlet UIImageView *photoPlaceholder;
@property (strong, nonatomic) IBOutlet UIButton *logoutButton;
@property (strong, nonatomic) IBOutlet UILabel *publishedLbl;
@property (strong, nonatomic) IBOutlet UILabel *unpublishedLbl;
@end
