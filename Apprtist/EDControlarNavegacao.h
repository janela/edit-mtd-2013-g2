//
//  EDControlarNavegacao.h
//  Apprtist
//
//  Created by Jorge Oliveira on 02/05/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EDControlarNavegacao : UINavigationController

-(IBAction)revealMenu:(id)sender;

@end
