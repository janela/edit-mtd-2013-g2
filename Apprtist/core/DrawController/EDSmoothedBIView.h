//
//  EDSmoothedBIView.h
//  FreehandDrawingTut
//
//  Created by A Khan on 12/10/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

//delegate to return amount entered by the user
@protocol DrawDelegate <NSObject>

-(void)drawElement:(UIBezierPath*)amount;


@end


@interface EDSmoothedBIView : UIView <UIApplicationDelegate>{
  //  id<DrawDelegate> delegate;
    
 
}

-(void)forceTouchMove:(NSSet *)touches withEvent:(UIEvent *)event;

@property(nonatomic,strong) UIImage * incrementalImage;

@property(nonatomic,strong) UIImage * maskImage;
@property(nonatomic,assign)id delegate;
@property(nonatomic,assign)BOOL activewrite;


@end
