//
//  EDBaseResponse.m
//  Apprtist
//
//  Created by Jorge Oliveira on 26/05/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import "EDBaseResponse.h"

#define kEDBaseResponse_DictionaryKey_Status @"Status"
#define kEDBaseResponse_DictionaryKey_Message @"Message"
#define kEDBaseResponse_DictionaryKey_Data @"Data"

@implementation EDBaseResponse

+ (EDBaseResponse*) parseResponseFromDictionary:(NSDictionary*)dictionary
{
    EDBaseResponse * result = [[EDBaseResponse alloc] init];
    result.data = [dictionary objectForKey:kEDBaseResponse_DictionaryKey_Data];

    return result;
}

@end
