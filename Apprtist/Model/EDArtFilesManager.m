//
//  EDArtFilesManager.m
//  Apprtist
//
//  Created by Jorge Oliveira on 26/05/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import "EDArtFilesManager.h"
#import "EDArtFilesStubDataSource.h"
#import "EDBaseResponse.h"

#define kEDArtworkManager_UseStubDataSource 1

@implementation EDArtFilesManager


- (void)getAllArtworksForUserId:(NSString*)userId
            withCompletionBlock:(void(^)(NSError* error, NSArray* artworks))completionBlock
{

		[_dataSource artworkManager:self
            getAllArtworksForUserId:userId
                withCompletionBlock:^(NSError *error, EDBaseResponse *artworks) {
                        completionBlock(error,artworks.data);
                    }];

}

- (void)getAllArtworksForCategory:(NSString*)category
            withCompletionBlock:(void(^)(NSError* error, NSArray* artworks))completionBlock
{
    
    [_dataSource artworkManager:self
      getAllArtworksForCategory:category
            withCompletionBlock:^(NSError *error, EDBaseResponse *artworks) {
                completionBlock(error,artworks.data);
            }];
    
}


- (void)getAllArtworksForArtID:(NSString*)artID
              withCompletionBlock:(void(^)(NSError* error, NSArray* artworks))completionBlock
{
    
    [_dataSource artworkManager:self
      getAllArtworksForArtID:artID
            withCompletionBlock:^(NSError *error, EDBaseResponse *artworks) {
                completionBlock(error,artworks.data);
            }];
    
}

- (void)getAllArtworks:(NSString*)artFiles
           withCompletionBlock:(void(^)(NSError* error, NSArray* artworks))completionBlock
{
    
    [_dataSource artworkManager:self
         getAllArtworks:nil
            withCompletionBlock:^(NSError *error, EDBaseResponse *artworks) {
                completionBlock(error,artworks.data);
            }];
    
}


#pragma mark -
#pragma mark Object Lifecycle

- (id)init
{
	self = [super init];
	if (self) {
		if(kEDArtworkManager_UseStubDataSource){
			_dataSource = [[EDArtFilesStubDataSource alloc] init];
            }else {
			//_dataSource = [[EDArtworkManagerDynamicDataSource alloc] init];
//			//NSAssert(false, @"No known implementation for EDArtworkManagerDataSource.");
		}
	}
	return self;
}

static EDArtFilesManager* _sharedManager;

+ (EDArtFilesManager *)sharedManager{
	if(!_sharedManager){
		_sharedManager = [[EDArtFilesManager alloc] init];
	}
	return _sharedManager;
}
@end
