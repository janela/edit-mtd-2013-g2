//
//  EDArtFilesManager.h
//  Apprtist
//
//  Created by Jorge Oliveira on 26/05/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EDArtFilesDataSourceManager.h"

@interface EDArtFilesManager : NSObject
{
	id<EDArtFilesDataSourceManager> _dataSource;
}


- (void)getAllArtworksForUserId:(NSString*)userId
            withCompletionBlock:(void(^)(NSError* error, NSArray* artworks))completionBlock;

-(void)getAllArtworksForCategory:(NSString *)category
             withCompletionBlock:(void (^)(NSError * error, NSArray *artworks))completionBlock;

- (void)getAllArtworksForArtID:(NSString*)artID
              withCompletionBlock:(void(^)(NSError* error, NSArray* artworks))completionBlock;

- (void)getAllArtworks:(NSString *)artFiles withCompletionBlock:(void(^)(NSError* error, NSArray* artworks))completionBlock;


+ (EDArtFilesManager*) sharedManager;


@end
