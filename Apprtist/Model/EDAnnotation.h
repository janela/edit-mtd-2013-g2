//
//  EDAnnotation.h
//  EDEXM3
//
//  Created by Jorge Oliveira on 16/05/13.
//  Copyright (c) 2013 a. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface EDAnnotation : NSObject<MKAnnotation>


@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;
@property (nonatomic, readwrite, copy) NSString *title;
@property (nonatomic, readwrite, copy) NSString *subtitle;

@property(nonatomic,assign) NSInteger * objectId;


@end
