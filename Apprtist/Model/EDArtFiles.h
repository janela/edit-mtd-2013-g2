//
//  EDArtFiles.h
//  Apprtist
//
//  Created by Jorge Oliveira on 20/05/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EDArtistas.h"

@class EDArtistas;

@interface EDArtFiles : NSObject

@property(nonatomic,strong) NSString * artID; //Not needed, remove... in code
@property(nonatomic,strong) NSString * artName; //title
@property(nonatomic,strong) NSString * artAuthorID; //Author ID
@property(nonatomic,strong) NSString * artAuthorFBID; //Author FB ID
//@property(nonatomic,strong) NSString * artDescription; //not needed
@property(nonatomic,strong) NSString * artFileURLLocation;//File
@property(nonatomic,strong) NSNumber * artLatitude;
@property(nonatomic,strong) NSNumber * artLongitude;
@property(nonatomic,strong) NSString * artCategory; //Tags
@property(nonatomic) BOOL artStatus;
@property(nonatomic,strong) NSDate * artDate;//Creation date

+ (NSArray*) parseObjectsFromDictionariesArray:(NSArray*)dictionariesArray;
+ (EDArtFiles*) parseObjectFromDictionary:(NSDictionary*)dictionary;

@end
