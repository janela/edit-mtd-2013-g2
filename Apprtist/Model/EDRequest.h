//
//  EDRequest.h
//  Apprtist
//
//  Created by Afonso Neto on 6/25/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EDRequest : NSObject <NSURLConnectionDelegate>
{
    NSMutableData *_responseData;
}

@property (strong, nonatomic) NSURLConnection *ligacao;
@property (strong, nonatomic) NSDictionary *dados;

-(void)fetchData:(NSData *)data;

- (NSURLRequest *)RequestAuthors;
- (NSURLRequest *)RequestTags;
- (NSURLRequest *)RequestTagsById:(NSString *)tagId;
- (NSURLRequest *)RequestAuthorsById:(NSString *)authorId;
- (NSURLRequest *)RequestWorksById:(NSString *)workId;
- (NSURLRequest *)RequestAuth;
- (NSURLRequest *)RequestWorksByWorkIds:(NSArray *)worksIds;
- (NSURLRequest *)RequestAllWorks;
- (NSURLRequest *) RequestToPublishWork:(NSString *)ficheiro withName:(NSString *)nomeImage andTags:(NSArray *)tagArray;

@end
